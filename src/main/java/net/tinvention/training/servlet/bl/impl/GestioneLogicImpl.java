package net.tinvention.training.servlet.bl.impl;

import java.sql.SQLException;
import java.util.List;

import javax.naming.NamingException;

import net.tinvention.training.servlet.bl.GestioneLogic;
import net.tinvention.training.servlet.dto.UtenteDto;
import net.tinvention.training.servlet.persistence.GestioneUtentiDao;

public class GestioneLogicImpl implements GestioneLogic {
	private GestioneUtentiDao gestione1;

	@Override
	public boolean esisteUsername(String username) throws ClassNotFoundException, SQLException, NamingException {
		return gestione1.esisteUsernameDb(username);
	}

	@Override
	public boolean esistepassword(String username, String password) throws ClassNotFoundException, SQLException {
		return gestione1.esistePasswordDb(username, password);
	}

	@Override
	public void modificaUsername(String username1, String username2) throws SQLException {
		gestione1.modificaUsernameDb(username1, username2);
	}

	@Override
	public void modificaPassword(String username, String password1, String password2) throws SQLException {
		gestione1.modificaPasswordDb(username, password1, password2);

	}

	@Override
	public int calcolaNumeroUtenti() throws SQLException {
		List<UtenteDto> lista = gestione1.restituisciListaUtenti();
		return lista.size();
	}

	@Override
	public double calcolaMediaEtaUtenti() throws Exception {
		List<Integer> lista = gestione1.restituisciEtaUtenti();
		double somma = 0;
		for (Integer u : lista) {
			somma += u;
		}
		return somma / lista.size();
	}

	@Override
	public double calcolaDeviazioneEtaUtenti() throws Exception {
		List<Integer> lista = gestione1.restituisciEtaUtenti();
		double somma = 0;
		double media = 0;
		for (Integer u : lista) {
			somma += u;
		}
		media = somma / lista.size();
		double dev = 0;
		for (Integer u : lista) {
			dev += Math.pow(u - media, 2);
		}
		return Math.sqrt(dev / (lista.size()));
	}

	@Override
	public void modificaEtaUtente(String username, int eta) throws SQLException {
		gestione1.modificaEtaDb(username, eta);
	}

	public GestioneUtentiDao getGestioneUtentiDao() {
		return gestione1;
	}

	public void setGestioneUtentiDao(GestioneUtentiDao gestioneUtentiDao) {
		this.gestione1 = gestioneUtentiDao;
	}

}
