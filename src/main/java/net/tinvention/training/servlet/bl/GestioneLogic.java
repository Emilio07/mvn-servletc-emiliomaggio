package net.tinvention.training.servlet.bl;

import java.sql.SQLException;
import javax.naming.NamingException;

public interface GestioneLogic {
	public boolean esisteUsername(String username) throws ClassNotFoundException, SQLException, NamingException;

	public boolean esistepassword(String username, String password) throws ClassNotFoundException, SQLException;

	public void modificaUsername(String username1, String username2) throws SQLException;

	public void modificaPassword(String username, String password1, String password2) throws SQLException;

	public int calcolaNumeroUtenti() throws SQLException;

	public double calcolaMediaEtaUtenti() throws SQLException, Exception;

	public double calcolaDeviazioneEtaUtenti() throws SQLException, Exception;

	public void modificaEtaUtente(String username, int eta) throws SQLException;
}
