package net.tinvention.training.servlet.conf;

import java.io.IOException;

import javax.naming.NamingException;

import net.tinvention.training.servlet.bl.impl.GestioneLogicImpl;
import net.tinvention.training.servlet.persistence.impl.GestioneCredenzialiDaoImpl;
import net.tinvention.training.servlet.persistence.impl.GestioneUtentiDaoImpl;

public class LayerConfigurer {
	public static GestioneLogicImpl getGestioneLogicImplConfigured() throws IOException, NamingException {
		GestioneLogicImpl gestioneLogic = new GestioneLogicImpl();
		GestioneCredenzialiDaoImpl gestioneCredenziali = new GestioneCredenzialiDaoImpl();
		GestioneUtentiDaoImpl gestioneUtentiDao = new GestioneUtentiDaoImpl(gestioneCredenziali.ottieniDataSource("src//main//resources//main.properties"));
		gestioneLogic.setGestioneUtentiDao(gestioneUtentiDao);
		return gestioneLogic;
	}
}
