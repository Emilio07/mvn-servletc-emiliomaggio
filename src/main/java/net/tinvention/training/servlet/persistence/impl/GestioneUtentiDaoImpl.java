package net.tinvention.training.servlet.persistence.impl;

import java.util.ArrayList;
import java.util.List;
import javax.naming.NamingException;
import javax.sql.DataSource;
import java.sql.Connection;

import net.tinvention.training.servlet.dto.UtenteDto;
import net.tinvention.training.servlet.persistence.GestioneUtentiDao;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class GestioneUtentiDaoImpl implements GestioneUtentiDao {
	private DataSource dataSource;

	public GestioneUtentiDaoImpl(DataSource dataSource) {
		this.dataSource = dataSource;
	}

	@Override
	public boolean esisteUsernameDb(String username) throws ClassNotFoundException, SQLException, NamingException {
		Connection conn = null;
		PreparedStatement prep = null;
		ResultSet res = null;
		try {
			conn = dataSource.getConnection();
			prep = conn.prepareStatement("select * from utenti where username=?;");
			prep.setString(1, username);
			res = prep.executeQuery();
			if (res.next()) {
				return true;
			} else {
				return false;
			}
		} finally {
			if (res != null) {
				res.close();
			}
			if (prep != null) {
				prep.close();
			}
			if (conn != null) {
				conn.close();
			}
		}

	}

	@Override
	public boolean esistePasswordDb(String username, String password) throws ClassNotFoundException, SQLException {
		Connection conn = null;
		PreparedStatement prep = null;
		ResultSet res = null;
		try {
			conn = dataSource.getConnection();
			prep = conn.prepareStatement("select * from utenti where username=? and password=?;");
			prep.setString(1, username);
			prep.setString(2, password);
			res = prep.executeQuery();
			if (res.next()) {
				return true;
			} else {
				return false;
			}
		} finally {
			if (res != null) {
				res.close();
			}
			if (prep != null) {
				prep.close();
			}
			if (conn != null) {
				conn.close();
			}
		}

	}

	@Override
	public void modificaUsernameDb(String username1, String username2) throws SQLException {
		Connection conn = null;
		PreparedStatement prep = null;
		try {
			conn = dataSource.getConnection();
			prep = conn.prepareStatement("update utenti set username=? WHERE username=?;");
			prep.setString(1, username2);
			prep.setString(2, username1);
			prep.executeUpdate();
		} finally {
			if (prep != null) {
				prep.close();
			}
			if (conn != null) {
				conn.close();
			}
		}
	}

	@Override
	public void modificaPasswordDb(String username, String password1, String password2) throws SQLException {
		try (Connection conn = dataSource.getConnection();
				PreparedStatement prep = conn
						.prepareStatement("update utenti set password= ? WHERE username=? AND password=?;");) {
			prep.setString(1, password2);
			prep.setString(2, username);
			prep.setString(3, password1);
			prep.executeUpdate();
		}

	}

	@Override
	public void modificaEtaDb(String username, int eta) throws SQLException {
		try (Connection conn = dataSource.getConnection();
				PreparedStatement prep = conn.prepareStatement("update utenti set et�= ? WHERE username=?;");) {
			prep.setInt(1, eta);
			prep.setString(2, username);
			prep.executeUpdate();
		}

	}

	@Override
	public List<UtenteDto> restituisciListaUtenti() throws SQLException {
		List<UtenteDto> lista = new ArrayList<>();
		Connection conn = null;
		PreparedStatement prep = null;
		try {
			conn = dataSource.getConnection();
			prep = conn.prepareStatement("select * from utenti;");
			try (ResultSet res = prep.executeQuery()) {
				while (res.next()) {
					lista.add(new UtenteDto(res.getString("username"), res.getString("password"), res.getInt("et�")));
				}
			}
			return lista;
		} finally {
			if (prep != null) {
				prep.close();
			}
			if (conn != null) {
				conn.close();
			}
		}
	}

	@Override
	public List<Integer> restituisciEtaUtenti() throws Exception {
		List<Integer> lista = new ArrayList<Integer>();
		try (Connection conn = dataSource.getConnection();
				PreparedStatement prep = conn.prepareStatement("select et� from utenti;")) {
			try (ResultSet res = prep.executeQuery()) {
				while (res.next()) {
					lista.add(res.getInt(1));
				}
			}
		}
		return lista;
	}

	public DataSource getDataSource() {
		return dataSource;
	}

}
