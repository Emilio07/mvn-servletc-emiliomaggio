/**
 * 
 */
package net.tinvention.training.servlet.persistence.impl;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import net.tinvention.training.servlet.persistence.GestioneCredenzialiDao;

/**
 * @author EmilioMaggio
 *
 */
public class GestioneCredenzialiDaoImpl implements GestioneCredenzialiDao {
	public DataSource ottieniDataSource(String file) throws IOException, NamingException {
		String resource = null;
		InputStream input = null;
		try {
			input = new FileInputStream(file);
			Properties prop = new Properties();
			prop.load(input);
			resource = prop.getProperty("RESOURCE");
			InitialContext init = new InitialContext();
			Context env = (Context) init.lookup("java:comp/env");
			DataSource dataSource = (DataSource) env.lookup(resource);
			return dataSource;
		} finally {
			if (input != null)
				input.close();
		}
	}
}