package net.tinvention.training.servlet.persistence;

import java.sql.SQLException;
import java.util.List;

import javax.naming.NamingException;

import net.tinvention.training.servlet.dto.UtenteDto;

public interface GestioneUtentiDao {
	public boolean esisteUsernameDb(String username) throws ClassNotFoundException, SQLException, NamingException;

	public boolean esistePasswordDb(String username, String password) throws ClassNotFoundException, SQLException;

	public void modificaUsernameDb(String username1, String username2) throws SQLException;

	public void modificaPasswordDb(String username, String password1, String password2) throws SQLException;

	public List<UtenteDto> restituisciListaUtenti() throws SQLException;

	void modificaEtaDb(String username, int eta) throws SQLException;

	List<Integer> restituisciEtaUtenti() throws Exception;
}