package net.tinvention.training.servlet.persistence;

import java.io.IOException;

import javax.naming.NamingException;
import javax.sql.DataSource;

public interface GestioneCredenzialiDao {
	public DataSource ottieniDataSource(String file) throws IOException, NamingException;
}
