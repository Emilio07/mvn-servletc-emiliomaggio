package net.tinvention.training.servlet.dto;

public class UtenteDto {
	private String password;
	private String username;
	private int eta;

	public UtenteDto() {
	};

	public UtenteDto(String username, String password, int eta) {
		this.username = username;
		this.password = password;
		this.eta = eta;
	}

	public String getUsername() {
		return username;
	}

	public String getPassword() {
		return password;
	}

	public int getEta() {
		return eta;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setEta(int eta) {
		this.eta = eta;
	}
}
