package net.tinvention.training.servlet.ui;

import java.io.*;
import java.text.MessageFormat;

import javax.servlet.*;
import javax.servlet.http.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import net.tinvention.training.servlet.bl.GestioneLogic;
import net.tinvention.training.servlet.conf.LayerConfigurer;

// Extend HttpServlet class
public class ModificaServlet extends HttpServlet {
	/**
		 * 
		 */
	private static final long serialVersionUID = -8212023877929124461L;
	private GestioneLogic gestione;
	private static final Logger LOG = LogManager.getLogger(ModificaServlet.class);
	private String username1;
	private String password1;
	private String username2;
	private String password2;
	private String eta2;
	private String message = "";

	public void init(ServletConfig sc) throws ServletException {
		try {
			super.init(sc);
			gestione = LayerConfigurer.getGestioneLogicImplConfigured();
		} catch (Exception e) {
			LOG.error("ERRORE durante la configurazione", e);
			throw new ServletException(e);
		}
	}

	protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		username1 = (String) req.getSession().getAttribute("username");
		password1 = (String) req.getSession().getAttribute("password");
		username2 = (String) req.getParameter("new username");
		password2 = (String) req.getParameter("new password");
		eta2 = (String) req.getParameter("new age");
		try {
			if ((username2.trim() != null) & (username2.trim() != "") & (!gestione.esisteUsername(username2))) {
				LOG.info("modifica di username in corso");
				req.getSession().setAttribute("username", username2);
				gestione.modificaUsername(username1, username2);
			}
			if ((password2.trim() != null) & (password2.trim() != "")
					& (!gestione.esistepassword(username1, password2))) {
				LOG.info("modifica di password in corso");
				req.getSession().setAttribute("password", password2);
				gestione.modificaPassword(username1, password1, password2);
			}
			if ((eta2.trim() != null) & (eta2.trim() != "")) {
				LOG.info("modifica di et� in corso");
				gestione.modificaEtaUtente(username1, Integer.parseInt(eta2));
			}
		} catch (Exception e) {
			// LOG.error("ClassNotFoundException occurred!", e1);
			res.sendRedirect("modifica?warnMessage=error in modify");
		}
		String string = "<!DOCTYPE html><html>" + "<head><title>Pagina modify</title></head>"
				+ "<body><header><h1>Modifiche effettuate!{0}</h1></header>" + "</body></html>";
		String string2 = MessageFormat.format(string, message);
		res.setContentType("text/html");
		res.setCharacterEncoding("UTF-8");
		res.getWriter().write(string2);

	}
}
