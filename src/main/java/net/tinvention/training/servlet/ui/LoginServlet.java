package net.tinvention.training.servlet.ui;

import java.io.*;
import java.sql.SQLException;

import javax.naming.NamingException;
import javax.servlet.*;
import javax.servlet.http.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import net.tinvention.training.servlet.bl.GestioneLogic;
import net.tinvention.training.servlet.conf.LayerConfigurer;

// Extend HttpServlet class
public class LoginServlet extends HttpServlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = -5635484287741042442L;
	/**
	 * 
	 */
	private GestioneLogic gestione;
	private static final Logger LOG = LogManager.getLogger(LoginServlet.class);

	public void init(ServletConfig sc) throws ServletException {
		try {
			super.init(sc);
			gestione = LayerConfigurer.getGestioneLogicImplConfigured();
		} catch (Exception e) {
			LOG.error("ERRORE durante inizializzazione", e);
			throw new ServletException(e);
		}
// Do required initialization

	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.sendRedirect("index.html?warnMessage=incorrect credentials");
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		try {
			HttpSession session = request.getSession(true);
			String username = request.getParameter("username");
			String password = request.getParameter("password");
			if (gestione.esisteUsername(username.trim()) & gestione.esistepassword(username.trim(), password.trim())) {
				LOG.info("L'utente si � autentificato correttamente!");
				session.setAttribute("username", username.trim());
				session.setAttribute("password", password.trim());
				getServletContext().getRequestDispatcher("/edit/*").forward(request, response);
			} else {
				LOG.info("L'utente non si � autentificato correttamente!");
				response.sendRedirect("login");
			}
		} catch (ClassNotFoundException e) {
			LOG.error("ClassNotFoundException occurred!", e);
		} catch (SQLException e) {
			LOG.error("SQLException occurred!", e);
		} catch (IOException e) {
			LOG.error("IOException occurred!", e);
		} catch (NamingException e) {
			LOG.error("NamingException occurred!", e);
		}
	}

	public void destroy() {
// do nothing.
	}
}