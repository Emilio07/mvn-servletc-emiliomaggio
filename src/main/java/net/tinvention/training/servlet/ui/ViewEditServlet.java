package net.tinvention.training.servlet.ui;

import java.io.IOException;
import java.sql.SQLException;
import java.text.MessageFormat;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import net.tinvention.training.servlet.bl.GestioneLogic;
import net.tinvention.training.servlet.conf.LayerConfigurer;

public class ViewEditServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6157059630496238989L;
	private static final Logger LOG = LogManager.getLogger();
	private GestioneLogic gestione;

	public void init(ServletConfig sc) throws ServletException {
		try {
			super.init(sc);
			gestione = LayerConfigurer.getGestioneLogicImplConfigured();
		} catch (Exception e) {
			// LOG.error("ERRORE durante inizializzazione", e);
			throw new ServletException(e);
		}
// Do required initialization

	}

	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		// Here i show the report and a link for data modification.
		try {
			int numeroUtenti = gestione.calcolaNumeroUtenti();
			double mediaEta = gestione.calcolaMediaEtaUtenti();
			double devEta = gestione.calcolaDeviazioneEtaUtenti();
			String stringPageTemplate = "	<!DOCTYPE HTML>" + "	<html>"
					+ "    <head><title>ViewEditServlet</title></head>" + "    	<body>"
					+ "<header> <h1>Autentificazione avvenuta con successo,� possibile modificare le credenziali</h1>"
					+ "<br /></header>" + "    	    <h2>EDIT PAGE</h2>" + "<br/>Il numero di utenti registrati: {0}"
					+ "         <br/>Et� media et� degli utenti: {1}" + "<br/>Deviazione standard et� degli utenti: {2}"
					+ "<p><strong>In seguito si possono modificare le credenziali,lasciare vuoti i campi da non modificare!</strong></p>"
					+ "         <br/>" + "<form action=\"modifica\" method=\"post\">"
					+ "username: <input type=\"text\" name=\"new username\""
					+ "placeholder=\"nuovo username\" size=30><br />"
					+ "password: <input type=\"text\" name=\"new password\" placeholder=\"nuova password\""
					+ "size=30><br />" + " et�:<input type=\"text\" name=\"new age\" placeholder=\"nuova et�\"><br />"
					+ "<input type=\"submit\" value=\"Modify\">" + "</form><br />" + "</body>" + "</html>";
			String outputBodycontent = MessageFormat.format(stringPageTemplate, numeroUtenti, mediaEta, devEta);
			resp.setContentType("text/html");
			resp.setCharacterEncoding("UTF-8");
			resp.getWriter().write(outputBodycontent);
			LOG.info("L'utente � su ViewEdit.");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
