package net.tinvention.training.servlet.persistence.impl;

import static org.junit.jupiter.api.Assertions.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.naming.NamingException;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import net.tinvention.training.servlet.conf.LayerConfigurerTest;
import net.tinvention.training.servlet.dto.UtenteDto;

class GestioneUtentiDaoImplTest {
	private static GestioneUtentiDaoImpl gestioneUtentiDao;

	@BeforeAll
	static void init() throws Exception {
		gestioneUtentiDao = LayerConfigurerTest.getGestioneUtentiDaoImplTest();
	}

	@BeforeEach
	void initAll() throws SQLException {
		try (Connection con = gestioneUtentiDao.getDataSource().getConnection();
				PreparedStatement prep = con.prepareStatement("insert into utenti VALUES (?,?,?);");) {
			prep.setString(1, "user");
			prep.setString(2, "pwd");
			prep.setInt(3, 20);
			prep.executeUpdate();
		}
	}

	@Test
	void esisteUsernameDb() throws ClassNotFoundException, SQLException, NamingException {
		assertTrue(gestioneUtentiDao.esisteUsernameDb("user"));
	}

	@Test
	void esistePasswordDb() throws ClassNotFoundException, SQLException {
		assertTrue(gestioneUtentiDao.esistePasswordDb("user", "pwd"));
	}

	@Test
	void modificaUsernameDb() throws ClassNotFoundException, SQLException {
		gestioneUtentiDao.modificaUsernameDb("user", "user1");
		try (Connection con = gestioneUtentiDao.getDataSource().getConnection();
				PreparedStatement prep = con.prepareStatement("select * from utenti where username='user1';");) {
			ResultSet res = prep.executeQuery();
			String s = "";
			if (res.next()) {
				s = res.getString("username");
			}
			assertEquals(s, "user1");
		}
	}

	@Test
	void modificaPasswordDb() throws ClassNotFoundException, SQLException {
		gestioneUtentiDao.modificaPasswordDb("user", "pwd", "pwd1");
		try (Connection con = gestioneUtentiDao.getDataSource().getConnection();
				PreparedStatement prep = con
						.prepareStatement("select * from utenti where username='user' and password='pwd1';");) {
			ResultSet res = prep.executeQuery();
			String s = "";
			if (res.next()) {
				s = res.getString("password");
			}
			assertEquals(s, "pwd1");
		}
	}

	@Test
	void modificaEtaDb() throws ClassNotFoundException, SQLException {
		gestioneUtentiDao.modificaEtaDb("user", 18);
		try (Connection con = gestioneUtentiDao.getDataSource().getConnection();
				PreparedStatement prep = con.prepareStatement("select * from utenti where username='user';");) {
			ResultSet res = prep.executeQuery();
			Integer e = 0;
			if (res.next()) {
				e = res.getInt("et�");
			}
			assertEquals(e, 18);
		}
	}

	@Test
	void restituisciListaUtentiTabellaDatabaseTest() throws Exception {
		List<UtenteDto> lista = new ArrayList<UtenteDto>();
		lista.add(new UtenteDto("user", "pwd", 20));
		assertTrue(gestioneUtentiDao.restituisciListaUtenti() instanceof List<?>);
		assertEquals(gestioneUtentiDao.restituisciListaUtenti().get(0).getUsername(), "user");
		assertEquals(gestioneUtentiDao.restituisciListaUtenti().get(0).getPassword(), "pwd");
		assertEquals(gestioneUtentiDao.restituisciListaUtenti().get(0).getEta(), 20);
	}

	@AfterEach
	void trillDown() throws SQLException {
		try (Connection con = gestioneUtentiDao.getDataSource().getConnection();
				PreparedStatement prep = con
						.prepareStatement("delete from utenti where username='user' or username='user1'");) {
			prep.executeUpdate();
		}
	}
}
