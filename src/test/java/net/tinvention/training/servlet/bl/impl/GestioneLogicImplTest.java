package net.tinvention.training.servlet.bl.impl;

import static org.junit.jupiter.api.Assertions.*;
import java.util.Arrays;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import net.tinvention.training.servlet.persistence.impl.GestioneUtentiDaoImpl;

@ExtendWith(MockitoExtension.class)
class GestioneLogicImplTest {
	@Mock
	private static GestioneUtentiDaoImpl gestioneUtentiDao;
	@InjectMocks
	private static GestioneLogicImpl gestioneLogic;

	@Test
	void calcolaMediaEtaUtentiTest() throws Exception {
		Mockito.when(gestioneUtentiDao.restituisciEtaUtenti()).thenReturn(Arrays.asList(20, 30));
		assertEquals(gestioneLogic.calcolaMediaEtaUtenti(), 25);
	}

	@Test
	void calcolaDeviazioneEtaUtentitest() throws Exception {
		Mockito.when(gestioneUtentiDao.restituisciEtaUtenti()).thenReturn(Arrays.asList(20, 30));
		assertEquals(gestioneLogic.calcolaDeviazioneEtaUtenti(), 5);
	}
}
