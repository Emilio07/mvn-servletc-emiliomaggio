package net.tinvention.training.servlet.conf;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import org.apache.commons.dbcp2.BasicDataSource;
import net.tinvention.training.servlet.bl.impl.GestioneLogicImpl;
import net.tinvention.training.servlet.persistence.impl.GestioneUtentiDaoImpl;

public class LayerConfigurerTest {
	public static GestioneLogicImpl getGestioneLogicImplConfigured() throws Exception {
		GestioneLogicImpl gestioneLogic = new GestioneLogicImpl();
		GestioneUtentiDaoImpl gestioneUtentiDao = new GestioneUtentiDaoImpl(ottieniDataSourceTest());
		gestioneLogic.setGestioneUtentiDao(gestioneUtentiDao);
		return gestioneLogic;
	}

	public static GestioneUtentiDaoImpl getGestioneUtentiDaoImplTest() throws Exception {
		GestioneUtentiDaoImpl gestioneUtenti = new GestioneUtentiDaoImpl(ottieniDataSourceTest());
		return gestioneUtenti;
	}

	public static BasicDataSource ottieniDataSourceTest() throws Exception {
		BasicDataSource ds = new BasicDataSource();
		final List<String> credenziali = leggiCredenzialiDaProperties("src//test//resources//test.properties");
		final String JDBC_URL = credenziali.get(0);
		final String JDBC_USERNAME = credenziali.get(1);
		final String JDBC_PWD = credenziali.get(2);
		ds.setUrl(JDBC_URL);
		ds.setUsername(JDBC_USERNAME);
		ds.setPassword(JDBC_PWD);
		return ds;
	}

	public static List<String> leggiCredenzialiDaProperties(String file) throws Exception {
		InputStream in = null;
		List<String> lista = new ArrayList<String>();
		Properties p = new Properties();
		try {
			in = new FileInputStream(file);
			p.load(in);
			lista.add(p.getProperty("JDBC_URL"));
			lista.add(p.getProperty("JDBC_USER"));
			lista.add(p.getProperty("JDBC_PASSWORD"));
			return lista;
		} finally {
			if (in != null) {
				in.close();
			}
		}
	}
}
